from washpy.permission import *


def test_profDosing_permission():
    """
    example from the Miele IP Profile documentation.
    """
    p = Permission(0x7108)

    assert p.service == Service.profDosing
    assert p.GET
    assert not p.PUT
    assert not p.POST
    assert not p.DELETE


def test_profPayment_permission():
    """
    example from the Miele IP Profile documentation.
    """
    p = Permission(0x6104)

    assert p.service == Service.profPayment
    assert not p.GET
    assert p.PUT
    assert not p.POST
    assert not p.DELETE
