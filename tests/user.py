import json
from washpy.user import User


def test_user_parsing():
    """
    Data captured in the wild.
    """
    json_data = '{"ID": 101, "LoginName": "Admin", "Password": "password", "Type": 1, "Description": "admin description", "Roles": [1, 2, 101, 102]}'
    data = json.loads(json_data)

    u = User(**data)

    assert u.ID == 101
    assert u.LoginName == "Admin"
    assert u.Password == "password"
    assert u.Type == 1
    assert u.Description == "admin description"
    assert u.Roles == [1, 2, 101, 102]
